package com.dao;

//import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Hospital;
//import com.model.Product;
@Repository
public interface HospitalRepository extends JpaRepository<Hospital,Integer> {

	@Query("from Hospital h where h.hospitalName = :hospitalName")
	Hospital findByName(@Param("hospitalName") String hospitalName);

	@Query("from Hospital h where h.emailId= :emailId and h.password= :password")
	Hospital hospitalLogin(@Param("emailId")String emailId,@Param("password") String password);
	
	
}
