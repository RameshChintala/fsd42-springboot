package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Department;
@Service
public class DepartmentDAO {
	@Autowired
	DepartmentRepository deptRepository;
	
	public List<Department> getAllDepartments() {
		return deptRepository.findAll();
	}

	public Department getDepartmentById( int deptId) {
		return deptRepository.findById(deptId).orElse(new Department(0,"Departments Not Found!!","Location Not Found"));
	}

	public Department getDepartmentByName(String deptName) {
		return deptRepository.findByName(deptName);
	}

	public Department registerDepartment(Department department) {
		return deptRepository.save(department);
	}

	public Department updateDepartment(Department department) {
		return deptRepository.save(department);
	}

	public void deleteDepartment(int deptId) {
		deptRepository.deleteById(deptId);
	}
}
