package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Organ;

@Service
public class OrganDAO {
	@Autowired
	OrganRepository organRepository;

	public List<Organ> getAllOrgans() {
		return organRepository.findAll();
	}

	public Organ getOrganById(int organId) {
		Organ organ = new Organ(0,"Organ not found!!","Not found!!",null,"Not found!!",0,"Not found!!");
		return organRepository.findById(organId).orElse(organ);
	}

	public Organ getOrganByName(String organName) {
		return organRepository.findByName(organName);
	}

	public Organ registerOrgan(Organ organ) {
		return organRepository.save(organ);
	}

	public Organ updateOrgan(Organ organ) {
		return organRepository.save(organ);
	}

	public void deleteOrgan(int organId) {
		organRepository.deleteById(organId);
		
	}

	
	
	
	
}
