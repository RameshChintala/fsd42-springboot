package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Hospital;
//import com.model.Product;
@Service
public class HospitalDAO {

	@Autowired
	HospitalRepository hospitalRepository;
	
	public List<Hospital> getAllHospitals() {
		return hospitalRepository.findAll();
	}

	public Hospital getHospitalById(int hospitalId) {
		Hospital hospital = new Hospital(0,"Hospital not found!!","Not found!!","Not found!!",0L,"Not found!!","Not found!!",null);
		return hospitalRepository.findById(hospitalId).orElse(hospital);
	}

	public Hospital getHospitalByName(String hospitalName) {
		return hospitalRepository.findByName(hospitalName);
	}

	public Hospital registerHospital(Hospital hospital) {
		return hospitalRepository.save(hospital);
	}

	public Hospital updateHospital(Hospital hospital) {
		return hospitalRepository.save(hospital);
	}

	public void deleteHospital(int hospitalId) {
		hospitalRepository.deleteById(hospitalId);
		
	}

	public Hospital hospitalLogin(String emailId, String password) {
		return hospitalRepository.hospitalLogin(emailId,password);
	}

	
}
