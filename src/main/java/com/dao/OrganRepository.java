package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Organ;

@Repository
public interface OrganRepository extends JpaRepository<Organ,Integer>{
	
	@Query("from Organ org where org.organName = :organName")
	Organ findByName(@Param("organName") String organName);
}
