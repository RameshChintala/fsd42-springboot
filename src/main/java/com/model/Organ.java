package com.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Organ {
	 @Id@GeneratedValue
    private int organId;
	
	private String organName;
	private String bloodgroup;
	private Date date;
	private String gender;
	private int age;
	private String filePath; 
	
	public Organ() {
		super();
	}

	public Organ(int organId, String organName, String bloodgroup, Date date, String gender, int age, String filePath) {
		super();
		this.organId = organId;
		this.organName = organName;
		this.bloodgroup = bloodgroup;
		this.date = date;
		this.gender = gender;
		this.age = age;
		this.filePath = filePath;
	}

	public int getOrganId() {
		return organId;
	}

	public void setOrganId(int organId) {
		this.organId = organId;
	}

	public String getOrganName() {
		return organName;
	}

	public void setOrganName(String organName) {
		this.organName = organName;
	}

	public String getBloodgroup() {
		return bloodgroup;
	}

	public void setBloodgroup(String bloodgroup) {
		this.bloodgroup = bloodgroup;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public String toString() {
		return "Organ [organId=" + organId + ", organName=" + organName + ", bloodgroup=" + bloodgroup + ", date="
				+ date + ", gender=" + gender + ", age=" + age + ", filePath=" + filePath + "]";
	}

	
	
}
