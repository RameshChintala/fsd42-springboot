package com.model;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Department {
	 @Id@GeneratedValue
	   private int deptId;
	   private String deptName;
	   private String location;
	   
	   @JsonIgnore
		@OneToMany(mappedBy="department")
		List<Hospital> hospitalList = new ArrayList<Hospital>();

		public Department(){
			super();
		}


		public Department(int deptId, String deptName, String location) {
			super();
			this.deptId = deptId;
			this.deptName = deptName;
			this.location = location;
		}

		public List<Hospital> getHospitalList() {
			return hospitalList;
		}

		public void setHospitalList(List<Hospital> hospitalList) {
			this.hospitalList = hospitalList;
		}
		
		public int getDeptId() {
			return deptId;
		}


		public void setDeptId(int deptId) {
			this.deptId = deptId;
		}


		public String getDeptName() {
			return deptName;
		}


		public void setDeptName(String deptName) {
			this.deptName = deptName;
		}


		public String getLocation() {
			return location;
		}


		public void setLocation(String location) {
			this.location = location;
		}


		@Override
		public String toString() {
			return "Department [deptId=" + deptId + ", deptName=" + deptName + ", location=" + location + "]";
		}
		
}
