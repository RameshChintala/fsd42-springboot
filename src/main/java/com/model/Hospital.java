package com.model;

import java.util.Date;

//import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Hospital {
	    @Id@GeneratedValue
		private int hospitalId;
	    
//	    @Column(name="productName", length=40)
		private String hospitalName;
		private String address;
		private String country;
		private long phonenumber;
		private String emailId;
		private String password;
		private Date date;
		
		@ManyToOne
		@JoinColumn(name="deptId")
		Department department;
		
		public Hospital() {
			super();
		}


		public Hospital(int hospitalId, String hospitalName, String address, String country, long phonenumber,
				String emailId, String password, Date date) {
			super();
			this.hospitalId = hospitalId;
			this.hospitalName = hospitalName;
			this.address = address;
			this.country = country;
			this.phonenumber = phonenumber;
			this.emailId = emailId;
			this.password = password;
			this.date = date;
		}

		public Department getDepartment() {
			return department;
		}

		public void setDepartment(Department department) {
			this.department = department;
		}


		public int getHospitalId() {
			return hospitalId;
		}


		public void setHospitalId(int hospitalId) {
			this.hospitalId = hospitalId;
		}


		public String getHospitalName() {
			return hospitalName;
		}


		public void setHospitalName(String hospitalName) {
			this.hospitalName = hospitalName;
		}


		public String getAddress() {
			return address;
		}


		public void setAddress(String address) {
			this.address = address;
		}


		public String getCountry() {
			return country;
		}


		public void setCountry(String country) {
			this.country = country;
		}


		public long getPhonenumber() {
			return phonenumber;
		}


		public void setPhonenumber(long phonenumber) {
			this.phonenumber = phonenumber;
		}


		public String getEmailId() {
			return emailId;
		}


		public void setEmailId(String emailId) {
			this.emailId = emailId;
		}


		public String getPassword() {
			return password;
		}


		public void setPassword(String password) {
			this.password = password;
		}


		public Date getDate() {
			return date;
		}


		public void setDate(Date date) {
			this.date = date;
		}


		@Override
		public String toString() {
			return "Hospital [hospitalId=" + hospitalId + ", hospitalName=" + hospitalName + ", address=" + address
					+ ", country=" + country + ", phonenumber=" + phonenumber + ", emailId=" + emailId + ", password="
					+ password + ", date=" + date +", department=" + department + "]";
		}


		
		
}
