package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.HospitalDAO;
import com.model.Hospital;
//import com.model.Product;
import com.model.Department;

@RestController
public class HospitalController {
	// Dependency Injection
		@Autowired
		HospitalDAO hospitalDAO;

		@GetMapping("/getAllHospitals")
		public List<Hospital> getAllHospitals() {
			return hospitalDAO.getAllHospitals();
		}
		
		@GetMapping("/getHospitalById/{hospitalId}")
		public Hospital getHospitalById(@PathVariable("hospitalId") int hospitalId) {
			return hospitalDAO.getHospitalById(hospitalId);
		}
	
		@GetMapping("/getHospitalByName/{hospitalName}")
		public Hospital getHospitalByName(@PathVariable("hospitalName") String hospitalName) {
			return hospitalDAO.getHospitalByName(hospitalName);
		}
		
		@PostMapping("registerHospital")
		public Hospital registerHospital(@RequestBody Hospital hospital) {
			return hospitalDAO.registerHospital(hospital);
		}
		
		@PutMapping("updateHospital")
		public Hospital updateHospital(@RequestBody Hospital hospital) {
			return hospitalDAO.updateHospital(hospital);
		}

		@DeleteMapping("deleteHospital/{hospitalId}")
		public String deleteHospital(@PathVariable("hospitalId") int hospitalId) {
			hospitalDAO.deleteHospital(hospitalId);
			return "Hospital with HospitalId: " + hospitalId + " Deleted Successfully!!!";
		}
		
		@GetMapping("/hospitalLogin/{emailId}/{password}")
		public Hospital hospitalLogin(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
			return hospitalDAO.hospitalLogin(emailId,password);
		}
}
