package com.ts;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.DepartmentDAO;
import com.model.Department;

@RestController
public class DepartmentController {
	  @Autowired
	   DepartmentDAO deptDAO;
	   
	   @GetMapping("/getAllDepartments")
	   public List<Department> getAllDepartments(){
		   return deptDAO.getAllDepartments();
	   }
	   
	   @GetMapping("/getDepartmentById/{deptId}")
	   public Department getDepartmentById(@PathVariable("deptId")int deptId){
		   return deptDAO.getDepartmentById(deptId);
	   }
	   
	   @GetMapping("/getDepartmentByName/{deptName}")
	   public Department getDepartmentByName(@PathVariable("deptName")String deptName){
		   return deptDAO.getDepartmentByName(deptName);
	   }
	   
	   @PostMapping("registerDepartment")
	   public Department registerDepartment(@RequestBody Department department){
		   return deptDAO.registerDepartment(department);
	   }
	   
	   @PutMapping("updateDepartment")
	   public Department updateDepartment(@RequestBody Department department){
		   return deptDAO.updateDepartment(department);
	   }
	   
	   @DeleteMapping("deleteDepartment/{deptId}")
	   public String deleteDepartment(@PathVariable("deptId")int deptId){
		   deptDAO.deleteDepartment(deptId);
		   return "Department with DepartmentId: " + deptId + " deleted successfully!!";
	   }
	   
}
