package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.dao.OrganDAO;
import com.model.Organ;

public class OrganController {
	@Autowired
	OrganDAO organDAO;
	
	@GetMapping("/getAllOrgans")
	public List<Organ> getAllOrgans() {
		return organDAO.getAllOrgans();
	}
	
	@GetMapping("/getOrganById/{organId}")
	public Organ getOrganById(@PathVariable("organId") int organId) {
		return organDAO.getOrganById(organId);
	}
	
	@GetMapping("/getOrganByName/{organName}")
	public Organ getHospitalByName(@PathVariable("organName") String organName) {
		return organDAO.getOrganByName(organName);
	}
	
	@PostMapping("registerOrgan")
	public Organ registerOrgan(@RequestBody Organ organ) {
		return organDAO.registerOrgan(organ);
	}
	
	@PutMapping("updateOrgan")
	public Organ updateOrgan(@RequestBody Organ organ) {
		return organDAO.updateOrgan(organ);
	}

	@DeleteMapping("deleteOrgan/{hospitalId}")
	public String deleteOrgan(@PathVariable("organId") int organId) {
		organDAO.deleteOrgan(organId);
		return "Organ with OrganId: " + organId + " Deleted Successfully!!!";
	}
	
	
	
	
}
